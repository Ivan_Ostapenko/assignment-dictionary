%define current 0										


%macro colon 2										 
	%ifid %2											
		%2: dq current									
		%define current %2								
	%else												
		%fatal "The second argument must be an id" 		
	%endif													
	%ifstr %1											
		db %1, 0										
	%else												
		%faral "The first argument must be a string"	
	%endif													
%endmacro	