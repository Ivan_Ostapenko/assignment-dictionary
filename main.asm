%define MAX_CAPACITY 256
%define STDOUT 1
%define STDERR 2
%define OFFSET 8

section .text

extern find_word
extern read_word
extern print_string
extern print_newline
extern string_length
extern exit
global _start

section .data

%include 'words.inc'

read_error: db 'Error. Can not read the word', 10, 0
not_found: db 'No entries have been found.', 10, 0

section .text

_start:			
	sub rsp, MAX_CAPACITY		
    mov rsi, MAX_CAPACITY		
    mov rdi, rsp				
    call read_word 				
    test rax, rax					
    jz .err_read 				
    mov rsi, current			
    mov rdi, rax 				
    call find_word				
    test rax, rax					
    je .not_found 				

.found:
   	add rax, OFFSET 			
    push rax 					
    mov rsi, rax				
    call string_length 			 
    pop rsi 					
    add rax, rsi				
    inc rax 					
	mov rdi, rax					
	mov rsi, STDOUT				
	call print_string 			 
	call print_newline 			
	jmp .end					

.err_read:
	mov rdi, read_error			
	mov rsi, STDERR				
	call print_string			
	jmp .end					

.not_found:
	mov rdi, not_found			
	mov rsi, STDERR				
	call print_string			
	jmp .end					

.end:
	add rsp, MAX_CAPACITY		
	call exit    				