section .text
global find_word

extern string_length
extern string_equals
find_word:
.loop:
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    cmp rax, 0
    jne .found

    mov rsi, [rsi]
    cmp rsi, 0
    je .not_found
    jmp .loop

.not_found:
    xor rax, rax
    ret

.found:
    mov rax, rsi
    ret

