NASM := nasm -felf64 -o
LD := ld -o
all: main.o lib.o dict.o
	$(LD) main.o lib.o dict.o lab2

main.o: colon.inc main.asm
	$(NASM) main.o main.asm

lib.o: lib.inc
	$(NASM) lib.o lib.inc

dict.o: dict.asm
	$(NASM) dict.o dict.asm

clean:
	rm -rf *.o lab2